/* Validation des données du formulaire pour l'input name */

// Selection de l'input name
let myInputName = document.querySelector('form #name');

// Selection de la span error-name
let errorInputName = document.querySelector('.error-name');

// Ecouteur d'évènement lors de l'input name
myInputName.addEventListener("input", function validateInputName() {

  // Vérification que la longueur du nom est de 50 caractères max puis je met une classe error-name à la span error
  if (myInputName.value.length < 51 & myInputName.value.length !== 0) {
    errorInputName.innerHTML = "";
    errorInputName.className = "error-name";
    // Je modifie la bordure en vert
    myInputName.style.border = "2px solid green";

} else {
  // Envoie un message d'erreur si > 50 caractères et je met une classe error-name-active à la span error
    errorInputName.innerHTML = "Votre nom d'utilisateur ne doit pas dépasser 50 caractères !";
    errorInputName.className = "error-name-active";
    // Je modifie la bordure en rouge
    myInputName.style.border = "2px solid red";
}
});

/* Validation des données du formulaire pour l'input email */

// Selection de l'input email
let myInputEmail = document.querySelector('form #email');

// Déclaration d'un regex pour validation d'email 
const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,4}))$/;

// Selection de la span error-email
let errorInputEmail = document.querySelector('.error-email');

// Ecouteur d'évènement lors de l'input email
myInputEmail.addEventListener("input", function validateInputEmail() {
  
  // Je récupère la valeur de l'input email
  let myInputEmailValue = myInputEmail.value;

  // Je vérifie si cette input respecte le regex défini. Dans ce cas, pas d'erreur et je met une classe error-email à la span error
  if (myInputEmailValue.match(regexEmail)) {
    errorInputEmail.innerHTML = "";
    errorInputEmail.className = "error-email";
    // Je modifie la bordure en vert
    myInputEmail.style.border = "2px solid  green";
  } else {
    // Sinon message d'error et je met une classe error-email-active à la span error
    errorInputEmail.innerHTML = "Votre adresse mail n'est pas valide !";
    errorInputEmail.className = "error-email-active";
    // Je modifie la bordure en rouge
    myInputEmail.style.border = "2px solid  red";
}
});


/* Validation des données du formulaire pour l'input phone */

// Selection de l'input phone
let myInputPhone = document.querySelector('form #phone');

// Déclaration d'un regex pour validation téléphone (06 ou 07) 
const regexPhone = /^(0)[67][0-9]{8}$/;

// Selection de la span error-phone
let errorInputPhone = document.querySelector('.error-phone');

// Ecouteur d'évènement lors de l'input phone
myInputPhone.addEventListener("input", function validateInputPhone() {

  // Je récupère la valeur de l'input phone
  let myInputPhoneValue = myInputPhone.value;

  // Je vérifie si cette input respecte le regex défini. Dans ce cas, pas d'erreur et je met une classe error-phone à la span error
  if (myInputPhoneValue.match(regexPhone)) {
    errorInputPhone.innerHTML = "";
    errorInputPhone.className = "error-phone";
    // Je modifie la bordure en vert
    myInputPhone.style.border = "2px solid green";
  } else {
    // Sinon message d'error et je met une classe error-phone-active à la span error
    errorInputPhone.innerHTML = "Votre numéro de téléphone n'est pas valide !";
    errorInputPhone.className = "error-phone-active";
    // Je modifie la bordure en vert
    myInputPhone.style.border = "2px solid red";
}
});


/* Validation des données du formulaire lors du submit */

// Selection du formulaire
let selectForm = document.querySelector('form');

// Selection de la div error-form
let errorInputForm = document.querySelector('.error-form');

// Ecouteur d'évènement lors du submit du formulaire
selectForm.addEventListener("submit", function(e) {
  
  // On empêche l'envoi des données du formulaire (comportement par défaut)
  e.preventDefault();

  // Chaque fois que l'utilisateur tente d'envoyer les données
  // on vérifie que les champs name, email et phone sont valides.

  // Vérification que les champs ne sont pas vides
  if (myInputName.value.length == 0) {
    myInputName.style.border = "2px solid red";
    errorInputName.innerHTML = "Le nom d'utilisateur n'a pas été rempli !";
    errorInputName.style.color = "red";
  } else if (myInputEmail.value.length == 0) {
    myInputEmail.style.border = "2px solid red"; 
    errorInputEmail.innerHTML = "L'email n'a pas été rempli !"; 
    errorInputEmail.style.color = "red";
  } else if (myInputPhone.value.length == 0) {
    errorInputPhone.innerHTML = "Le numéro de téléphone n'a pas été rempli !"; 
    myInputPhone.style.border = "2px solid red"; 
    errorInputPhone.style.color = "red"; 
  } else {
  // Si les champs ne sont pas vides et valides, on n'affiche pas de message d'erreur
  if (errorInputName.className == "error-name" && errorInputEmail.className == "error-email" && errorInputPhone.className == "error-phone") {
    errorInputForm.innerHTML = "<strong>Votre demande d'inscription a bien été prise en compte</strong>";
    errorInputForm.className = "error-form";
  } else {
    // Si les champs sont invalides, on affiche un message d'erreur
    errorInputForm.innerHTML = "<strong>Les champs remplis ne sont pas valides. Veuillez reessayer.</strong>";
    errorInputForm.className = "error-form-active";
  }  
  }
});
